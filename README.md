# LDAP JDBC Tomcat Realm

Tomcat realm used when authentication is done against an LDAP server 
and authorization is done against the database. As suggested by gchabala
on the link below.

http://stackoverflow.com/questions/1138450/implement-a-tomcat-realm-with-ldap-authentication-and-jdbc-authorization

Doubts on Java/Gradle projects:

https://docs.gradle.org/current/userguide/tutorial_java_projects.html

## Configuration

Put the jar file at `TOMCAT_HOME/lib` and put the below configuration at server.xml inside `<Engine>` tag.

```
<Realm className="org.apache.catalina.realm.LdapJdbcRealm"
    connectionURL="ldap://ldap.lesteti.com.br"
    connectionName="CN=tomcat,OU=Leste TI,DC=lesteti,DC=com,DC=br"
    connectionPassword="12345"
    referrals="follow"
    userBase="DC=lesteti,DC=com,DC=br"
    userSubtree="true"
    userSearch="(sAMAccountName={0})"
    dataSourceName="jdbc/datasourceSecurity"
    connectionTimeout="50000"
    localDataSource="false"
    userTable="tomcat_user_role" userNameCol="user_name"
    userRoleTable="tomcat_user_role" roleNameCol="role_name" />
```

## Setting Up Tomcat For Remote Debugging

Extracted from [here](https://confluence.sakaiproject.org/display/BOOT/Setting+Up+Tomcat+For+Remote+Debugging).

Tomcat can be configured to allow a program such as eclipse to connect 
remotely using JPDA and see debugging information.
To configure tomcat to allow remote debugging, start tomcat using the 
catalina startup script (from your tomcat home) instead of the normal 
startup script like so (tomcat must be stopped before you can change over):

Windows:
```
set JPDA_ADDRESS=8000
set JPDA_TRANSPORT=dt_socket
bin/catalina.bat jpda start
```

Unix:
```
export JPDA_ADDRESS=8000
export JPDA_TRANSPORT=dt_socket
bin/catalina.sh jpda start
```

Use remote debugging from your favorite IDE to connect to Tomcat.
