package org.apache.catalina.realm;

import org.apache.catalina.*;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.deploy.SecurityConstraint;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.ietf.jgss.GSSContext;

import javax.naming.directory.DirContext;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.security.Principal;
import java.security.cert.X509Certificate;
import java.util.List;

/**
 * Tomcat realm used when authentication is done against an LDAP server
 * and authorization is done against the database.
 *
 * @author leonardobrasileiro@gmail.com
 */
public class LdapJdbcRealm implements Realm, Lifecycle {

    private static final Log log = LogFactory.getLog(LdapJdbcRealm.class);

    /**
     * Encapsulated <b>DataSourceRealm</b> to do role lookups
     */
    private DataSourceRealm dataSourceRealm = new DataSourceRealm();

    /**
     * Encapsulated <b>JNDIRealm</b> to do role lookups
     */
    private JNDIRealm jndiRealm = new JNDIRealm();

    /**
     * Descriptive information about this <b>Realm</b> implementation.
     */
    protected static final String info = "org.apache.catalina.realm.LdapJdbcRealm/1.0";

    /**
     * Descriptive information about this <b>Realm</b> implementation.
     */
    protected static final String name = "LdapJdbcRealm";

    /**
     * Set the all roles mode.
     *
     * @param allRolesMode authentication mode
     */
    public void setAllRolesMode(String allRolesMode) {
        dataSourceRealm.setAllRolesMode(allRolesMode);
    }

    /**
     * Return the data source name to use to connect to the database.
     *
     * @return dataSourceName
     * @see DataSourceRealm#getDataSourceName()
     */
    public String getDataSourceName() {
        return dataSourceRealm.getDataSourceName();
    }

    /**
     * Set the data source name to use to connect to the database.
     *
     * @param dataSourceName
     * @see DataSourceRealm#setDataSourceName(String)
     */
    public void setDataSourceName(String dataSourceName) {
        dataSourceRealm.setDataSourceName(dataSourceName);
    }

    public boolean getLocalDataSource() {
        return dataSourceRealm.getLocalDataSource();
    }

    public void setLocalDataSource(boolean localDataSource) {
        dataSourceRealm.setLocalDataSource(localDataSource);
    }

    /**
     * Return the table that holds user data..
     *
     * @return table name
     * @see DataSourceRealm#getUserTable()
     */
    public String getUserTable() {
        return dataSourceRealm.getUserTable();
    }

    /**
     * Set the table that holds user data.
     *
     * @param userTable The table name
     * @see DataSourceRealm#setUserTable(String)
     */
    public void setUserTable(String userTable) {
        dataSourceRealm.setUserTable(userTable);
    }

    /**
     * Return the column in the user table that holds the user's name.
     *
     * @return username database column name
     * @see DataSourceRealm#getUserNameCol()
     */
    public String getUserNameCol() {
        return dataSourceRealm.getUserNameCol();
    }

    /**
     * Set the column in the user table that holds the user's name.
     *
     * @param userNameCol The column name
     * @see DataSourceRealm#setUserNameCol(String)
     */
    public void setUserNameCol(String userNameCol) {
        dataSourceRealm.setUserNameCol(userNameCol);
    }

    /**
     * Return the table that holds the relation between user's and roles.
     *
     * @return user role database table name
     * @see DataSourceRealm#getUserRoleTable()
     */
    public String getUserRoleTable() {
        return dataSourceRealm.getUserRoleTable();
    }

    /**
     * Set the table that holds the relation between user's and roles.
     *
     * @param userRoleTable The table name
     * @see DataSourceRealm#setUserRoleTable(String)
     */
    public void setUserRoleTable(String userRoleTable) {
        dataSourceRealm.setUserRoleTable(userRoleTable);
    }

    /**
     * Return the column in the user role table that names a role.
     *
     * @return role column name
     * @see DataSourceRealm#getRoleNameCol()
     */
    public String getRoleNameCol() {
        return dataSourceRealm.getRoleNameCol();
    }

    /**
     * Set the column in the user role table that names a role.
     *
     * @param roleNameCol The column name
     * @see DataSourceRealm#setRoleNameCol(String)
     */
    public void setRoleNameCol(String roleNameCol) {
        dataSourceRealm.setRoleNameCol(roleNameCol);
    }

    @Override
    public Container getContainer() {
        return jndiRealm.getContainer();
    }

    @Override
    public void setContainer(Container container) {
        jndiRealm.setContainer(container);
        dataSourceRealm.setContainer(container);
    }

    @Override
    public String getInfo() {
        return info;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        jndiRealm.addPropertyChangeListener(listener);
        dataSourceRealm.addPropertyChangeListener(listener);
    }

    @Override
    public Principal authenticate(String username) {
        log.debug("authenticate(" + username + ")");
        return jndiRealm.authenticate(username);
    }

    @Override
    public Principal authenticate(String username, String credentials) {
        log.debug("authenticate(" + username + ", credentials)");
        Principal p = (GenericPrincipal) jndiRealm.authenticate(username, credentials);
        return p == null ? null : new GenericPrincipal(username, credentials, getRoles(username));
    }

    @Override
    public Principal authenticate(String username, String digest, String nonce, String nc, String cnonce, String qop, String realm, String md5a2) {
        return jndiRealm.authenticate(username, digest, nonce, nc, cnonce, qop, realm, md5a2);
    }

    @Override
    public Principal authenticate(GSSContext gssContext, boolean storeCreds) {
        return jndiRealm.authenticate(gssContext, storeCreds);
    }

    @Override
    public Principal authenticate(X509Certificate[] certs) {
        return authenticate(certs);
    }

    @Override
    public void backgroundProcess() {
        jndiRealm.backgroundProcess();
    }

    @Override
    public SecurityConstraint[] findSecurityConstraints(Request request, Context context) {
        return dataSourceRealm.findSecurityConstraints(request, context);
    }

    @Override
    public boolean hasUserDataPermission(Request request, Response response,
                                         SecurityConstraint []constraints) throws IOException {
        return dataSourceRealm.hasUserDataPermission(request, response, constraints);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        jndiRealm.removePropertyChangeListener(listener);
        dataSourceRealm.removePropertyChangeListener(listener);
    }

    protected String getName() {
        return jndiRealm.getName();
    }

    protected String getPassword(String username) {
        return jndiRealm.getPassword(username);
    }

    protected Principal getPrincipal(String username) {
        return jndiRealm.getPrincipal(username);
    }

    @Override
    public boolean hasResourcePermission(Request request, Response response,
                                         SecurityConstraint[]constraints,
                                         Context context) throws IOException {
        return dataSourceRealm.hasResourcePermission(request, response, constraints, context);
    }

    public boolean hasRole(Wrapper wrapper, Principal principal, String role) {
        return dataSourceRealm.hasRole(wrapper, principal, role);
    }

    /**
     * Return a List of roles associated with the given User. If no roles
     * are associated with this user, a zero-length List is returned.
     *
     * @param context unused. JDBC does not need this field.
     * @param user The User to be checked
     * @return list of role names
     *
     * @see DataSourceRealm#getRoles(String)
     */
    protected List<String> getRoles(DirContext context, User user) {
        return getRoles(user.getUsername());
    }

    protected List<String> getRoles(String username) {
        return dataSourceRealm.getRoles(username);
    }

    @Override
    public void addLifecycleListener(LifecycleListener listener) {
        jndiRealm.addLifecycleListener(listener);
        dataSourceRealm.addLifecycleListener(listener);
    }

    @Override
    public LifecycleListener[] findLifecycleListeners() {
        return jndiRealm.findLifecycleListeners();
    }

    @Override
    public void removeLifecycleListener(LifecycleListener listener) {
        jndiRealm.removeLifecycleListener(listener);
        dataSourceRealm.removeLifecycleListener(listener);
    }

    @Override
    public void init() throws LifecycleException {
        jndiRealm.init();
        dataSourceRealm.init();
    }

    @Override
    public void start() throws LifecycleException {
        jndiRealm.start();
        dataSourceRealm.start();
    }

    @Override
    public void stop() throws LifecycleException {
        jndiRealm.stop();
        dataSourceRealm.stop();
    }

    @Override
    public void destroy() throws LifecycleException {
        jndiRealm.destroy();
        dataSourceRealm.destroy();
    }

    @Override
    public LifecycleState getState() {
        return jndiRealm.getState();
    }

    @Override
    public String getStateName() {
        return jndiRealm.getStateName();
    }

    public String getConnectionURL() {
        return jndiRealm.getConnectionURL();
    }

    public void setConnectionURL(String connectionURL) {
        jndiRealm.setConnectionURL(connectionURL);
    }

    public String getConnectionName() {
        return jndiRealm.getConnectionName();
    }

    public void setConnectionName(String connectionName) {
        jndiRealm.setConnectionName(connectionName);
    }

    public String getConnectionPassword() {
        return jndiRealm.getConnectionPassword();
    }

    public void setConnectionPassword(String connectionPassword) {
        jndiRealm.setConnectionPassword(connectionPassword);
    }

    public String getReferrals() {
        return jndiRealm.getReferrals();
    }

    public void setReferrals(String referrals) {
        jndiRealm.setReferrals(referrals);
    }

    public String getUserBase() {
        return jndiRealm.getUserBase();
    }

    public void setUserBase(String userBase) {
        jndiRealm.setUserBase(userBase);
    }

    public String getUserSearch() {
        return jndiRealm.getUserSearch();
    }

    public void setUserSearch(String userSearch) {
        jndiRealm.setUserSearch(userSearch);
    }

    public boolean getUserSubtree() {
        return jndiRealm.getUserSubtree();
    }

    public void setUserSubtree(boolean userSubtree) {
        jndiRealm.setUserSubtree(userSubtree);
    }

    public String getConnectionTimeout() {
        return jndiRealm.getConnectionTimeout();
    }

    public void setConnectionTimeout(String timeout) {
        jndiRealm.setConnectionTimeout(timeout);
    }

}
